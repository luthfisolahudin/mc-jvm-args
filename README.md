Repository ini berisi argument [Java](https://www.java.com/) untuk bermain [Minecraft](https://www.minecraft.net/) dengan performa yang lebih optimal.

---

Gunakan flag sesuai dengan versi yang akan dimainkan.

---

Resources:
  - [brucethemoose's Minecraft Performance Flags Benchmarks](https://github.com/brucethemoose/Minecraft-Performance-Flags-Benchmarks#readme)
  - [Oracle GraalVM Enterprise Edition](https://www.oracle.com/tools/technologies/graalvm-enterprise-edition.html)
